#ifndef RD53B_ANALOG_MONITOR_H
#define RD53B_ANALOG_MONITOR_H

// labremote
class AD799X;
class FT232H;

// std/stl
#include <memory>  // unique_ptr

// json
#include <nlohmann/json.hpp>

// FT232 GPIO PINS
#define PIN_CONV 4
#define PIN_ALERT 5
#define PIN_EFUSE_EN 6
#define PIN_TRIM0_EN 7
#define PIN_TRIM1_EN 8
#define PIN_TRIM2_EN 9
#define PIN_TRIM3_EN 10
#define PIN_VDD_SHUNT_EN 11

// ANALOG MONITOR CARD
#define ADC_MAX_COUNTS 4096

// SCC NTC
// datasheet: https://www.mouser.com/datasheet/2/315/AUA0000C8-1131141.pdf
// these values are for ERT-J0EM103J
// It is unclear if on the RD53B SCC we have: ERT-J0EM103J, ERT-J0EG103FA, or
// ERT-J0ER103J, which all have different beta values
#define NTC_R_REF 10000  // ohms
#define NTC_T_REF 25     // celsius
#define NTC_BETA 3900    // ERT-J0EM103J
//#define NTC_BETA 3380 // ERT-J0EG103FA
//#define NTC_BETA 4250 // ERT-J0ER103J

class RD53BAnalogMonitor {
 public:
    RD53BAnalogMonitor();
    ~RD53BAnalogMonitor();

    // FTDI open and loading of configuration
    bool init(std::string config_file);

    // initiate a single-shot ADC conversion manually
    void start_conversion();

    // initiate an ADC sampling of quantity \"name\"
    int32_t readCount(std::string name);
    double read(std::string name);

    // initiate an ADC sampling of quantities \"quantities\"
    // (uses mode 2 of AD7998 to read all channels)
    std::map<std::string, int32_t> readCount(
        const std::vector<std::string> quantities);
    std::map<std::string, double> read(
        const std::vector<std::string> quantities);

    std::vector<std::string> get_list_of_quantities();
    std::map<std::string, std::pair<uint32_t, uint32_t> > get_channel_map() {
        return m_channel_map;
    }

    // convert NTC measurements to temperature values
    float ntc_counts_to_temp(int32_t adc_counts, bool in_kelvin = false);
    float ntc_to_temp(double value, bool in_kelvin = false);

 private:
    std::shared_ptr<FT232H> m_ft232;
    std::shared_ptr<AD799X> m_adc0;
    std::shared_ptr<AD799X> m_adc1;

    float m_adc_vref;

    std::map<std::string, std::pair<uint32_t, uint32_t> > m_channel_map;
    bool valid_channel(std::string name);
    uint32_t name_to_channel(std::string name);

    void get_adc_channels_and_fields(
        int adc_idx, const std::vector<std::string> input_fields,
        std::vector<uint8_t>& adc_channels, std::vector<std::string>& fields);

    float get_ntc_temp(float r_ntc, bool in_kelvin);
    float r_ntc_from_counts(int32_t counts);
    float r_ntc_from_value(double value);
    float get_ntc_temp_steinhart(float r_ntc, bool in_kelvin);

    // I2C addresses of the two ADCs on the analog monitor card
    uint32_t m_adc_i2c_address_0 = 0x21; // part U2
    uint32_t m_adc_i2c_address_1 = 0x22; // part U3

    float m_ntc_r_ref;  // Ohms
    std::vector<float> m_steinhart_coeffs;
};

#endif  // ANALOG_MONITOR_H
