// anamon
#include "FileUtils.h"
#include "Monitoring.h"
#include "RD53BAnalogMonitor.h"

// labremote
#include "Logger.h"

// std/stl
#include <iostream>
#include <string>
//#include <memory> // unique_ptr
#include <getopt.h>

#include <iomanip>

struct option longopts_t[]{{"config", required_argument, NULL, 'c'},
                           {"run", required_argument, NULL, 'r'},
                           {"temp", required_argument, NULL, 't'},
                           {"no-ps", no_argument, NULL, 'p'},
                           {"list", required_argument, NULL, 'l'},
                           {"meas", required_argument, NULL, 'm'},
                           {"help", no_argument, NULL, 'h'},
                           {"debug", no_argument, NULL, 'd'},
                           {0, 0, 0, 0}};

void help() {
    std::cerr << "-------------------------------------------------------------"
                 "--------------------"
              << std::endl;
    std::cerr << " ana-mon " << std::endl;
    std::cerr << std::endl;
    std::cerr << " Usage: ana-mon [options]" << std::endl;
    std::cerr << std::endl;
    std::cerr << " Options:" << std::endl;
    std::cerr << "  -c|--config          : Provide JSON configuration for SCC "
                 "analog monitor card"
              << std::endl;
    std::cerr << "  -r|--run             : Provide a monitoring config and "
                 "start monitoring"
              << std::endl;
    std::cerr << "  -t|--temp            : Provide a temperature (if climate "
                 "controlled) in degrees Celsius"
              << std::endl;
    std::cerr << "  --no-ps              : Do not monitor powersupply channels"
              << std::endl;
    std::cerr << "  -l|--list            : List the quantities that can be "
                 "measured and exit"
              << std::endl;
    std::cerr << "  -m|--meas [STRING]   : Perform a measurement of a "
                 "specified quantity and exit"
              << std::endl;
    std::cerr << "  -d|--debug           : Increase log verbosity" << std::endl;
    std::cerr << "  -h|--help            : Print this help message and exit"
              << std::endl;
    std::cerr << "-------------------------------------------------------------"
                 "--------------------"
              << std::endl;
}

int main(int argc, char* argv[]) {
    std::string anamon_config_file = "";
    std::string measure_quantity = "";
    bool do_list = false;
    std::string monitor_config_filename = "";
    bool run_monitor = false;
    float temp_set_input = -400.0;
    bool temp_provided = false;
    bool ignore_ps_monitoring = false;

    int c;
    while ((c = getopt_long(argc, argv, "c:dhlm:r:t:p", longopts_t, NULL)) !=
           -1) {
        switch (c) {
            case 'c':
                anamon_config_file = optarg;
                break;
            case 'd':
                logIt::incrDebug();
                logIt::incrDebug();
                logIt::incrDebug();
                logIt::incrDebug();
                break;
            case 'h':
                help();
                return 0;
            case 'l':
                do_list = true;
                break;
            case 'm':
                measure_quantity = optarg;
                break;
            case 'r':
                monitor_config_filename = optarg;
                break;
            case 'p':
                ignore_ps_monitoring = true;
                break;
            case 't':
                temp_set_input = std::stof(optarg);
                temp_provided = true;
                break;
            case '?':
                logger(logERROR) << "Invalid command-line argument provided";
                return 1;
        }  // switch
    }      // while

    //
    // check inputs
    //
    if (!anamon_config_file.empty()) {
        if (!anamon::utils::path_exists(anamon_config_file)) {
            logger(logERROR)
                << "Provided SCC analog monitor card configuration file (=\""
                << anamon_config_file << "\") could not be found";
            return 1;
        }
    } else {
        logger(logERROR)
            << "No SCC analog monitor card configuration file provided!";
        return 1;
    }

    if (!monitor_config_filename.empty()) {
        if (!anamon::utils::path_exists(monitor_config_filename)) {
            logger(logERROR)
                << "Provided monitor config file (=\""
                << monitor_config_filename << "\") could not be found";
            return 1;
        }
        run_monitor = true;
    }

    //
    // valid climate temperature?
    //
    if (temp_provided) {
        if (temp_set_input < -273.15) {
            logger(logERROR)
                << "Provided climate temperature (=" << temp_set_input
                << ") is an invalid Celsius temperature";
            return 1;
        }
    }

    //
    // setup the analog monitor card interface
    //
    RD53BAnalogMonitor anamon;
    if (!anamon.init(anamon_config_file)) {
        return 1;
    }

    if (run_monitor) {
        try {
            anamon::run(anamon, monitor_config_filename, temp_set_input,
                        ignore_ps_monitoring);
        } catch (std::exception& e) {
            logger(logERROR)
                << "Monitoring exited with exception: " << e.what();
            return 1;
        }
        return 0;
    }

    //
    // list and exit
    //
    if (do_list) {
        auto ordered_quantities = anamon.get_list_of_quantities();
        auto channel_map = anamon.get_channel_map();
        logger(logINFO) << "-----------------------------";
        for (size_t i = 0; i < ordered_quantities.size(); i++) {
            std::string name = ordered_quantities.at(i);
            auto channel_desc = channel_map.at(name);
            uint32_t adc_idx = std::get<0>(channel_desc);
            uint32_t adc_channel = std::get<1>(channel_desc);
            logger(logINFO)
                << "[" << std::setw(2) << i << "] " << std::setw(10) << name
                << " (ADC " << adc_idx << ", ch " << adc_channel << ")";
        }  // i
        logger(logINFO) << "-----------------------------";
        return 0;
    }

    //
    // measure
    //
    if (!measure_quantity.empty()) {
        int32_t measurement_counts = anamon.readCount(measure_quantity);
        if (measurement_counts < 0) {
            logger(logERROR) << "Invalid ADC measurement obtained ("
                             << measurement_counts << ")";
            return -1;
        }
        double measurement_value = anamon.read(measure_quantity);
        bool is_temp = false;
        if (measure_quantity.compare("TP_NTC1") == 0) {
            is_temp = true;
            measurement_value =
                anamon.ntc_to_temp(measurement_value, false /*in_kelvin*/);
        }
        logger(logINFO) << "Measurement: " << measure_quantity << " = "
                        << std::dec << measurement_counts << " counts "
                        << "(" << measurement_value << " "
                        << (is_temp ? "C" : "V") << ")";
        return 0;
    }

    return 0;
}
