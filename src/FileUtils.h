#ifndef ANAMON_FILEUTILS_H
#define ANAMON_FILEUTILS_H

// std/stl
#include <string>

namespace anamon {
namespace utils {

std::string anamon_dir();
std::string dir_date_name(std::string base_dir_name, std::string suffix = "");
std::string create_default_data_dir(std::string suffix = "");
bool create_dir_symlink(std::string existing_dir, std::string sym_link_name);
std::string replace_substr(std::string in, std::string old_sub,
                           std::string new_sub);
bool path_exists(std::string dir_name);

};  // namespace utils
};  // namespace anamon

#endif
