#ifndef RD53B_MONITORING_H
#define RD53B_MONITORING_H

// std/stl
#include <string>

// anamon
class RD53BAnalogMonitor;

namespace anamon {

void run(RD53BAnalogMonitor& monitor, std::string config_file,
         float climate_temp = -1000 /*degC*/, bool ignore_ps = false);
void close(int signal);

};  // namespace anamon

#endif
