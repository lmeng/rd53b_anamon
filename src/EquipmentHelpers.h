#ifndef ANAMON_EQUIPMENT_HELPERS_H
#define ANAMON_EQUIPMENT_HELPERS_H

// labremote
class IPowerSupply;
class PowerSupplyChannel;

// std/stl
#include <memory>
#include <string>
#include <vector>

// json
#include <nlohmann/json.hpp>

namespace anamon {
namespace equipment {

std::shared_ptr<IPowerSupply> ps_init(nlohmann::json config,
                                      std::string ps_name);
std::shared_ptr<PowerSupplyChannel> ps_channel_init(
    std::shared_ptr<IPowerSupply> ps, std::string ps_name, unsigned channel);

};  // namespace equipment
};  // namespace anamon

#endif
