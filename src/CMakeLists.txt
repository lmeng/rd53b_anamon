##
## libftdi1 and libmpsse are required for RD53BAnalogMonitor
##
find_package(libftdi1)
find_package(libmpsse)

# this is picked up from labremote
if ( (${LIBFTDI1_FOUND}) AND (${LIBMPSSE_FOUND}) )
    message("FTDI libs OK")
    message("Setting LIBFTDI1=1")
    add_definitions(-DLIBFTDI1=1)
else()
    message(FATAL_ERROR "Could not find FTDI or MPSSE libraries (LIBFTDI1_FOUND=${LIBFTDI1_FOUND} and LIBMPSSE_FOUND=${LIBMPSSE_FOUND})")
endif()

##
## library definition
##
add_library(RD53BAnalogMonitor SHARED)
target_sources(RD53BAnalogMonitor
    PRIVATE
    RD53BAnalogMonitor.cpp
    Monitoring.cpp
    FileUtils.cpp
    EquipmentHelpers.cpp
)

target_link_libraries(RD53BAnalogMonitor PUBLIC Com PS Meter Load DevCom Utils EquipConf)
target_include_directories(RD53BAnalogMonitor PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})

list(APPEND LABREMOTELIBS "Com;DataSink;DevCom;EquipConf;PS;Utils")
foreach(lib ${LABREMOTELIBS})
    target_include_directories(RD53BAnalogMonitor PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/../labRemote/src/lib${lib})
endforeach()

add_subdirectory(tools)
