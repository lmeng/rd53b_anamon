#include "EquipmentHelpers.h"

// labremote
#include "EquipConf.h"
#include "IPowerSupply.h"
#include "Logger.h"
#include "PowerSupplyChannel.h"

// std/stl
#include <algorithm>

namespace anamon {
namespace equipment {

std::shared_ptr<IPowerSupply> ps_init(nlohmann::json config,
                                      std::string ps_name) {
    logger(logDEBUG) << "Initializing power-supply \"" << ps_name << "\"...";
    std::shared_ptr<EquipConf> hw = std::make_shared<EquipConf>();
    hw->setHardwareConfig(config);
    return hw->getPowerSupply(ps_name);
}

std::shared_ptr<PowerSupplyChannel> ps_channel_init(
    std::shared_ptr<IPowerSupply> ps, std::string ps_name, unsigned channel) {
    logger(logDEBUG) << "Initializing power-supply channel " << channel;
    std::shared_ptr<PowerSupplyChannel> ps_channel =
        std::make_shared<PowerSupplyChannel>(ps_name + std::to_string(channel),
                                             ps, channel);
    return ps_channel;
}

};  // namespace equipment
};  // namespace anamon
