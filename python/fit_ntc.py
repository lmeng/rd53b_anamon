#!/bin/env python
from argparse import ArgumentParser
import sys

import numpy as np
import matplotlib.pyplot as plt

from math import log
from scipy.optimize import curve_fit

def steinhart_func(x, a, b, c) :
    inv_t = a + b * np.log(x) + c * np.power(np.log(x), 3)
    return inv_t

def main() :

    parser = ArgumentParser(description = "Determine Steinhart coefficients from T/R data")
    parser.add_argument("-f", "--input-file", required = True,
            help = "Text file containing temperature (in degrees Celsius data in first column and resistance (kOhm) data in second column"
    )
    parser.add_argument("-p", "--plot", action = "store_true",
            help = "Plot the data and post-fit Steinhart formula"
    )
    parser.add_argument("--it", action = "store_true",
            help = "Show plots interactively when plotting"
    )
    args = parser.parse_args()

    x_vals = []
    y_vals = []
    with open(args.input_file, "r") as infile :
        for line in infile :
            line = line.strip()
            if line.startswith("#") : continue
            fields = [float(x) for x in line.split()]
            temp = fields[0] # Celsius
            #if temp > 60 : continue
            resistance = fields[1] # in kOhm
            resistance *= 1000 # in Ohms
            x_vals.append(resistance)
            y_vals.append(temp + 273.15)
    x_vals = np.array(x_vals)
    y_vals = np.array(y_vals)

    ##
    ## fit
    ##
    popt, pcov = curve_fit(steinhart_func, x_vals, 1.0 / y_vals)

    param_names = ["A", "B", "C"]
    params = zip(param_names, popt)
    print(f"Steinhart fit results:")
    for i in params :
        print(f" {i[0]} = {i[1]}")

    if args.plot :
        fig, ax = plt.subplots(1,1)
        ax.tick_params(axis = "both", which = "both", labelsize = 16, direction = "in",
                labelleft = True, bottom = True, top = True, right = True, left = True)
        ax.grid(color = "k", which = "both", linestyle = "--", lw = 0.5, alpha = 0.1)
        ax.set_xlabel(r"R [$\Omega$]", horizontalalignment = "right", x = 1)
        ax.set_ylabel("T [K]", horizontalalignment = "right", y = 1)
        ax.set_xscale("log")
        ax.plot(x_vals, y_vals, "ko", markersize = 2, label = "Input Data")
        fit_label = f"Steinhart Fit (A={popt[0]:.4e},B={popt[1]:.4e},C={popt[2]:.4e})"
        ax.plot(x_vals, 1.0 / steinhart_func(x_vals, *popt), "r-", label = fit_label)

        ax.legend(loc = "best", frameon = False)

        if args.it :
            fig.show()
            x = input()
        out_name = args.input_file.split("/")[-1].replace(".txt", ".pdf")
        fig.savefig(out_name, bbox_inches = "tight")
        print(f"Figure saved: {out_name}")

if __name__ == "__main__" :
    main()
